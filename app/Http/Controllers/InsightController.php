<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InsightController extends Controller
{
    public function index()
    {
        $data['businessList'] = DB::table('Business')->get();
        $data['plantList'] = DB::table('Plant')->get();
        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function productionInsight(Request $request)
    {
        $userId = Auth::user()->UserID;
        $menuId = "13";
        $data = DB::select("SELECT * FROM vwParamDefaultSlab WHERE UserID=$userId AND MenuId=$menuId");
        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function filterChart(Request $request)
    {
        $business = $request->business;
        $plant = $request->plant;
        $startDate = $request->startDate;
        $particulars = $request->particulars;
        $title = '';
        foreach ($particulars as $key => $particular) {
            $title .= $key + 1;
            $title .= '#'.$particular['title'];
            $title .= '#'.$particular['min'];
            $title .= '#'.$particular['max'];
            if ($key+1 != count($particulars)) $title .= ',';
        }

        $data = DB::select("EXEC SP_AgingHistogram '$startDate' ,'$title','$business','$plant'");
        // $data = DB::select("EXEC SP_AgingHistogram 'Jan 15 2023' ,'1#Fresh#0#10,2#Mature#10#40,3#old#40#360++','P','P003');
        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }
}
