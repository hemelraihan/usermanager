<?php

namespace App\Http\Controllers;

use App\Models\OutletIP;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

class InventoryController extends Controller
{
    public function inventoryDetails(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'sessionId' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => 'Invalid'], 400);
        }

        try {
            ini_set('max_execution_time', 0);
            $user = Auth::user();
            $outlet = $user->OutletCode;
            (new SettingController())->dbConnection($outlet);
            $conn = DB::connection('sqlsrv_eps');
            try {
                DB::connection('sqlsrv_eps')->getPdo();
            } catch (\Exception $e) {
                return response()->json(['message' => "Could not connect to the database.  Please check your configuration. error:" . $e], 400);
            }
//            $startDate = $request->date[0];
//            $endDate = $request->date[1];
            $session = $request->sessionId;
            $user = $request->user;
            $location = $request->location == 'L3' ? '' : $request->location;
            // $sql = "SP_StockCountInventoryDetails '$startDate','$endDate','$outlet','$location','$user'";
            $sql = "SP_StockCountInventoryDetails '$session','$outlet','$location','$user', ''";
            $pdo = $conn->getPdo()->prepare($sql);
            $pdo->execute();
            $rows = $pdo->fetchAll(\PDO::FETCH_ASSOC);
            return response()->json(['details' => $rows], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => "Oops! Something Went Wrong"], 400);
        }

    }

    public function closeSession($id)
    {
        try {
            ini_set('max_execution_time', 0);
            $user = Auth::user();
            $outlet = $user->OutletCode;
            (new SettingController())->dbConnection($outlet);
            $conn = DB::connection('sqlsrv_eps');
            try {
                DB::connection('sqlsrv_eps')->getPdo();
            } catch (\Exception $e) {
                return response()->json(['message' => "Could not connect to the database.  Please check your configuration. error:" . $e], 400);
            }
            $sql = "UPDATE StockCountMaster SET  Active = 'N' WHERE Active = 'Y' AND (StockCountID = '$id' OR StockCountDateTo < GETDATE())";
            $pdo = $conn->getPdo()->prepare($sql);
            $pdo->execute();
            return response()->json(['message' => 'Session End Successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => "Oops! Something Went Wrong"], 400);
        }
    }

    public function getActiveSession()
    {
        try {
            ini_set('max_execution_time', 0);
            $user = Auth::user();
            $outlet = $user->OutletCode;
            (new SettingController())->dbConnection($outlet);
            $conn = DB::connection('sqlsrv_eps');
            try {
                DB::connection('sqlsrv_eps')->getPdo();
            } catch (\Exception $e) {
                return response()->json(['message' => "Could not connect to the database.  Please check your configuration. error:" . $e], 400);
            }
            $sql = "SELECT * FROM StockCountMaster WHERE Active = 'Y' AND DepotCode = '$outlet'";
            $pdo = $conn->getPdo()->prepare($sql);
            $pdo->execute();
            $rows = $pdo->fetchAll(\PDO::FETCH_ASSOC);

            return response()->json(['sessions' => $rows], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => "Oops! Something Went Wrong"], 400);
        }
    }

    public function getSupportingData()
    {
        try {
            ini_set('max_execution_time', 0);
            $user = Auth::user();
            $outlet = $user->OutletCode;
            (new SettingController())->dbConnection($outlet);
            $conn = DB::connection('sqlsrv_eps');
            try {
                DB::connection('sqlsrv_eps')->getPdo();
            } catch (\Exception $e) {
                return response()->json(['message' => "Could not connect to the database.  Please check your configuration. error:" . $e], 400);
            }
            $sql = "EXEC SP_StockCountReportSupporting '$outlet'";
            $pdo = $conn->getPdo()->prepare($sql);
            $pdo->execute();
            $res = array();
            do {
                $rows = $pdo->fetchAll(\PDO::FETCH_ASSOC);
                array_push($res, $rows);
            } while ($pdo->nextRowset());
            return response()->json(['data' => [$res[0], $res[1], $res[2]]], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => "Oops! Something Went Wrong"], 400);
        }
    }
}
