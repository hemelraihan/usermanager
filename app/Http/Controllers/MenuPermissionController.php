<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\UserApp;
use App\Models\UserMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuPermissionController extends Controller
{
    public function getUserMenuPermission(Request $request)
    {
        $data['menu'] = Menu::select('MenuID', 'MenuName')->Where('AppName', $request->appName)->orderBy('MenuOrder', 'asc')->get();
        $data['usermenu'] = UserMenu::where('UserID', $request->userId)->pluck('RefID');
        return $data;
    }

    public function saveUserMenuPermission(Request $request)
    {
        try {
            $userID = $request->userId;
            $permission = $request->permission;
            $sortedPerm = [];
            $userApp = UserApp::where('UserID',$userID)->where('AppName',$request->appName)->exists();
            if($userApp){
                return response()->json(['message' => "Permission already given for this App"], 400);
            }
            else{
                UserApp::create(['UserID' => $userID, 'AppName' => $request->appName]);
            }

            foreach ($permission as $key => $value) {
                if ($value) array_push($sortedPerm, $key);
            }
            DB::beginTransaction();
            $current = UserMenu::where('UserID', $userID)->pluck('RefID')->toArray();
            $inserted = array_diff($sortedPerm, $current);
            foreach ($inserted as $item) {
                UserMenu::create(['UserID' => $userID, 'AppName' => $request->appName, 'MenuType' => 'parent', 'RefID' => $item]);
            }
            $remove = array_diff($current, $sortedPerm);
            UserMenu::where('UserID', $userID)->whereIn('RefID', $remove)->delete();
           
            DB::commit();
            return response()->json(['message' => "Menu permissions updated Successfully"]);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => "Oops! Something Went Wrong"], 400);
        }
    }
}
