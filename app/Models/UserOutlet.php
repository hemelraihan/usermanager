<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserOutlet extends Model
{
    use HasFactory;

    public $timestamps = false;
    public $primaryKey = false;
    protected $guarded = [];
    public $incrementing = false;
    protected $table = "UserOutlet";

}
