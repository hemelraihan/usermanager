<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    protected $connection = "sqlsrv";
    protected $table = "Menu";
    public $primaryKey = 'ID';
    protected $guarded = [];
    public $timestamps = false;
    
    public function menuItem()
    {
        return $this->hasMany(MenuItem::class, 'MenuId', 'ID');
    }
}
