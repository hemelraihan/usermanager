<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserApp extends Model
{
    use HasFactory;
    protected $connection = "sqlsrv";
    protected $table = "UserApp";
    public $timestamps = false;
    public $primaryKey = false;
    public $incrementing = false;
    protected $guarded = [];
}
