<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\AuthController;
use \App\Http\Controllers\SettingController;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\MenuPermissionController;
use \App\Http\Controllers\HomeController;
use \App\Http\Controllers\InventoryController;
use \App\Http\Controllers\InsightController;

Route::post('login', [AuthController::class, 'login']);
Route::get('get-outlet/{user}', [HomeController::class, 'getOutlet']);

Route::group(['middleware' => 'jwt:api'], function () {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
//    Route::post('change-profile', [AuthController::class, 'changeProfile']);
  //  Route::post('change-password', [AuthController::class, 'changePass']);
    Route::get('app-supporting-data', [SettingController::class, 'appSupportingData']);

    Route::get('dashboard-data', [HomeController::class, 'dashboardData']);
    Route::get('get-inventory-filtering-data', [InventoryController::class, 'getSupportingData']);
    Route::post('get-inventory-details', [InventoryController::class, 'inventoryDetails']);

    // end session api
    Route::get('get-active-session', [InventoryController::class, 'getActiveSession']);
    Route::get('close-session/{id}', [InventoryController::class, 'closeSession']);
});


Route::group(['middleware' => ['jwt:api']], function () {
    // user
    Route::post('check-users', [UserController::class, 'checkUser']);
    Route::post('add-user', [UserController::class, 'store']);
    Route::post('update-user', [UserController::class, 'update']);
    Route::post('users', [UserController::class, 'index']);
    Route::delete('users/{id}', [UserController::class, 'delete']);
    Route::get('users', [UserController::class, 'allUsers']);
    Route::post('production-insight',[InsightController::class,'productionInsight']);

    // menu permissions
    Route::post('get-user-menu-details', [MenuPermissionController::class, 'getUserMenuPermission']);
    Route::post('save-user-menu-permission', [MenuPermissionController::class, 'saveUserMenuPermission']);
});

Route::group(['middleware' => ['jwt:api'],'prefix' => 'insights'], function () {
    Route::get('data',[InsightController::class,'index']);
    Route::get('production-insight',[InsightController::class,'productionInsight']);
    Route::post('filter-chart',[InsightController::class,'filterChart']);
});
