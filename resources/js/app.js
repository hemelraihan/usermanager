
require('./bootstrap');
require('./validation/index');

import Vue from 'vue'
import App from './views/App'
import router from './router/index';
import store from './store/index'
import {baseurl} from './base_url'

// main origin
Vue.prototype.mainOrigin = baseurl

import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
Vue.use(Toaster, {timeout: 5000})

export const bus = new Vue();


Vue.component('skeleton-loader', require('./components/loaders/Straight').default);
Vue.component('submit-form', require('./components/buttons/Submit').default);
Vue.component('submit-form-2', require('./components/buttons/Submit2').default);
Vue.component('datatable', require('./components/datatable/Index').default);
Vue.component('advanced-datatable', require('./components/datatable/Advanced').default);
Vue.component('data-export', require('./components/datatable/Export').default);
Vue.component('breadcrumb', require('./components/layouts/Breadcrumb').default);
// Vue.component('barchart', require('./components/chart/Bar').default);

//user
Vue.component('add-edit-user',require('./components/users/AddEditModal').default)

//Menu-Permission
Vue.component('menu-tree-view', require('./components/menu-permission/Index').default);

const app = new Vue({
    el: '#app',
    store: store,
    components: {App},
    router,
});
